require_dependency 'httparty'

module APIResponses
    class PurdueAPICalls < ApplicationController
        def get_details(filters)
            api_response = HTTParty.get('http://api.purdue.io/odata/' + filters )
            @json_hash = api_response.parsed_response
            return @json_hash
        end
    end
end