module MeetingsHelper
    def getBuildingNames
        buildingsHash = @api_responder.get_details('Buildings?$select=Name&$filter=CampusId eq 983c3fdc-f3f0-4f0b-a31c-c6f417e186fd')
        buildingNames = Array.new
        
        for i in 0..(buildingsHash["value"].count - 1)
            buildingNames << buildingsHash["value"][i]["Name"]
        end
        buildingNames.sort_by! { |e| e.downcase }
        buildingNames << "Other"
    end
end

