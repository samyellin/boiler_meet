module GroupsHelper
    def userInGroup
        @group.users.where(:id => current_user.id).present?
    end
    
    def isOwner?(id)
        @group.user_id == id
    end
end
