class GroupsController < ApplicationController
    layout "groups"
    before_action :authenticate_user!
    before_action :valid_user, only: [:destroy, :edit]
    
    def index
        @groups = current_user.groups.all.paginate(:page => params[:page])
        @usergroups = current_user.groups.all
    end

    def search
        @user = current_user
        @usergroups = current_user.groups.all
        @groups = Group.all.paginate(:page => params[:page])
        @responder = APIResponses::PurdueAPICalls.new.get_details('Courses')
    end
    

    def show
        @group = Group.find(params[:id])
        @usergroups = current_user.groups.all
        @members = @group.users.all
        @meetings = @group.meetings.all
    end
  
    def new
        @group = Group.new
        @usergroups = current_user.groups.all
    end
    
    def edit
        @group = Group.find(params[:id])
        @usergroups = current_user.groups.all
    end

    def update
        @group = Group.find(params[:id])
        @usergroups = current_user.groups.all
        if @group.update_attributes(group_params)
            flash[:success] = "Group updated"
            redirect_to @group
        else
        render 'edit'
        end
    end
  
    def create
        @group = Group.new(group_params)
        @usergroups = current_user.groups.all
        if @group.save
            @group.update_attribute(:user_id,current_user.id)
            @group.users << current_user
            flash[:success] = "Group Created"
            redirect_to groups_url
        else
            render 'new'
        end
    end

    def destroy
        @group = Group.find(params[:id])
        @usergroups = current_user.groups.all
        @group.destroy
        flash[:success] = "Group Deleted"
        redirect_to groups_url
    end
  
    def addUser
        @group = Group.find(params[:group_id])
        @usergroups = current_user.groups.all
        if @group.users.all.count >= @group.max_members
            flash[:notice] = "This group is full."
        else    
            @group.users << current_user
            flash[:success] = "You have joined the group!"
        end
        redirect_to group_url(@group)
    end
    
    def removeUser
        @group = Group.find(params[:group_id])
        @usergroups = current_user.groups.all
        @user = User.find(params[:user_id])
        if @user.id == current_user.id
            if (current_user.admin || current_user.id = @group.user_id && current_user.id != @group.user_id)
                @group.users.delete(@user)
                flash[:success] = "You have left the group"
                redirect_to groups_url
            else
                flash[:notice] = "You cannot do that."
                redirect_to group_url(@group)
            end
        else 
            @group.users.delete(@user)
            flash[:success] = @user.first_name + " has been removed from the group."
            redirect_to group_url(@group)
        end
    end
    
    def result
        @usergroups = current_user.groups.all
        if params[:search] == ""
            @groups = Group.search "*"
        else
            @groups = Group.search params[:search],  page: params[:page], per_page: 5
        end
    end
    
    def valid_user
        @group = Group.find(params[:id])
        @usergroups = current_user.groups.all
        unless current_user.admin || @group.user_id == current_user.id
            flash[:notice] = "You do not have permission to perform that action."
            redirect_to groups_url
        end
    end

    private
    
        def group_params
            params.require(:group).permit(:name, :description, :max_members)
        end
end
