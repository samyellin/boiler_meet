class MeetingsController < ApplicationController
  layout "groups"
  before_action :authenticate_user!
  before_action :valid_user, only: [:destroy, :edit]
  
  def new
    @group = Group.find(params[:group_id])
    @meeting = @group.meetings.build
    @api_responder = APIResponses::PurdueAPICalls.new
    roomsHash = @api_responder.get_details("Buildings?$expand=Rooms&$select=Name&$filter=Name eq 'ADM Agricultural Innovation Ct'")
    roomsArray = Array.new
    for i in 0..(roomsHash["value"][0]["Rooms"].count - 1)
        roomsArray << roomsHash["value"][0]["Rooms"][i]["Number"]
    end
    @rooms = roomsArray
  end

  def create
    @group = Group.find(params[:group_id])
    @meeting = @group.meetings.build(meeting_params)
    if @meeting.save
      flash[:success] = "Meeting created!"
      redirect_to groups_url
    else
      redirect_to groups_url
    end
  end

  def edit
    @meeting = Meeting.find(params[:id])
    @api_responder = APIResponses::PurdueAPICalls.new
    roomsHash = @api_responder.get_details("Buildings?$expand=Rooms&$select=Name&$filter=Name eq 'ADM Agricultural Innovation Ct'")
    roomsArray = Array.new
    for i in 0..(roomsHash["value"][0]["Rooms"].count - 1)
        roomsArray << roomsHash["value"][0]["Rooms"][i]["Number"]
    end
    @rooms = roomsArray
  end
  
  def update
    @meeting = Meeting.find(params[:id])
    if @meeting.update_attributes(meeting_params)
      flash[:success] = "Group updated"
      redirect_to @meeting.group
    else
      render 'edit'
    end
  end

  def destroy
    @group = Group.find(params[:group_id])
    @meeting = @group.meetings.find(params[:meeting_id])
    @meeting.destroy
    redirect_to group_url(@group)
  end
  
  def valid_user
    @group = Group.find(params[:group_id])
    unless current_user.admin || @group.user_id == current_user.id
      flash[:notice] = "You do not have permission to perform that action."
      redirect_to group_url(@group)
    end
  end

  def update_rooms
    @api_responder = APIResponses::PurdueAPICalls.new
    newBuildingName = params[:buildingName]
    roomsHash = @api_responder.get_details("Buildings?$expand=Rooms&$select=Name&$filter=Name eq '" + newBuildingName + "'")
    roomsArray = Array.new
    for i in 0..(roomsHash["value"][0]["Rooms"].count - 1)
        roomsArray << roomsHash["value"][0]["Rooms"][i]["Number"]
    end
    roomsArray << "Other"
    @rooms = roomsArray
    
    respond_to do |format|
      format.js
    end
  end

  private

    def meeting_params
      params.require(:meeting).permit(:building, :room, :purpose, :time)
    end
end
