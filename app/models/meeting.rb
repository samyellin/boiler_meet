class Meeting < ActiveRecord::Base
    belongs_to :group
    validates :purpose, presence:true, length: {maximum: 1000 }
    validates :time, presence:true
    validates :group_id, presence: true
end
