class Group < ActiveRecord::Base
    has_and_belongs_to_many :users, -> { uniq }
    has_many :meetings, dependent: :destroy
    
    searchkick
    
    validates :name, presence: true, length: { maximum: 100 }, uniqueness: true
    validates :description, presence:true
    validates :max_members, presence:true, numericality: { only_integer: true }
end
