$(document).on "ready" , ->
    $('#description-tab').click (event) ->
        event.preventDefault()
        $('#meetings').hide()
        $('#members').hide()
        $('#group-description').fadeIn()

        
    $('#meetings-tab').click (event) ->
        event.preventDefault()
        $('#group-description').hide()
        $('#members').hide()
        $('#meetings').fadeIn()
        
    $('#members-tab').click (event) ->
        event.preventDefault()
        $('#group-description').hide()
        $('#meetings').hide()
        $('#members').fadeIn()
    
    $("#my-menu").mmenu 
        'offCanvas': 'position': 'right'
        'classNames': 
            'vertical': "expand"
            'divider': "title"
