# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $(document).on 'change', '#building-select', (evt) ->
    if /new/.test(window.location.pathname)
      routeName = 'update_rooms'
    else
      routeName = 'edit_rooms'
    $.ajax routeName,
      type: 'GET'
      dataType: 'script'
      data: {
        buildingName: $("#building-select option:selected").val()
      }
      error: (jqXHR, textStatus, errorThrown) ->
        console.log("AJAX Error: #{textStatus}")
      success: (data, textStatus, jqXHR) ->
        console.log("Dynamic building select OK!")
        
    if $('#building-select option:selected').val() == 'Other' 
      $('#other-room-select').empty()
      $('#other-building-select').append("<input type='text' name='meeting[building]' id='meeting_building' placeholder='Building'></input>")
      $('#meeting_room').prop("disabled", true)
    else 
      $('#other-building-select').empty()
      $('#meeting_room').prop("disabled", false)
      
    if $('#building-select option:selected').val() == 'Other'
      $('#other-room-select').empty()
      $('#other-room-select').append("<input type='text' name='meeting[room]' id='meeting_room' placeholder='Room'></input>")
      $('#meeting_room').prop("disabled", true)
      $('#room-select').hide()
    else 
      $('#other-room-select').empty()
      $('#meeting_room').prop("disabled", false)
      $('#room-select').show()
    
    $(document).on 'change', '#room-select', (evt) ->
      if $('#room-select option:selected').val() == 'Other' and $('#building-select option:selected').val() != 'Other'   
        $('#other-room-select').empty()
        $('#other-room-select').append("<input type='text' name='meeting[room]' id='meeting_room' placeholder='Room'></input>")
      else
        $('#other-room-select').empty()    
    
  $(document).on 'ready' , ->      
    $('#other-info').qtip({
        content: {
            text: "No problem! Select 'other' and enter your custom location!"
        }
        position: {
          my: 'center left',
          at: 'center right',
          target: $('#other-info')
      }
    })    