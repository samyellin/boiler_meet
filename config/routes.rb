Rails.application.routes.draw do
  devise_for :users
  root 'home#index'
  get 'groups' => 'groups#index'
  get 'contact' => 'home#contact'
  get 'search' => 'groups#search'
  get 'result' => 'groups#result'
  get 'meetings/update_rooms', as: 'update_rooms'
  get 'meetings/:id/edit_rooms', to:'meetings#update_rooms', as: 'edit_rooms'
  
  resources :groups
  resources :meetings
  
  resources :groups do
      member do
          post :addUser 
          post :removeUser
      end
  end
end
