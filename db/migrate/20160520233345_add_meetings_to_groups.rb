class AddMeetingsToGroups < ActiveRecord::Migration
  def change
      add_column :meetings, :group_id, :integer
      add_index 'meetings', ['group_id'], :name => 'meetings_group_id' 
  end
end
