class AddToMeetings < ActiveRecord::Migration
  def change
    add_column :meetings, :location, :string
    add_column :meetings, :purpose, :text
    add_column :meetings, :time, :datetime
  end
end
