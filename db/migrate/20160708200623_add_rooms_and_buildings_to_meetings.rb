class AddRoomsAndBuildingsToMeetings < ActiveRecord::Migration
  def change
    rename_column :meetings, :location, :building
    add_column :meetings, :room, :string
  end
end
